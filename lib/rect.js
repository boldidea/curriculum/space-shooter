/**
 * A Rect is used for storing rectangular coordinates. It represents a width, height, 
 * and position. A Rect's position can be updated through any one if its coordinate 
 * properties.
 * @param {Number} left 
 * @param {Number} top 
 * @param {Number} width 
 * @param {Number} height 
 */
function Rect(left, top, width, height) {
    this.x = left;
    this.y = top;
    this.width = width;
    this.height = height;

    Object.defineProperties({
        top: {
            get: function() {
                return this.y;
            },
            set: function(value) {
                this.y = value;
            }
        },
        right: {
            get: function() {
                return this.x + this.width;
            },
            set: function(value) {
                this.x = value - this.width;
            }
        },
        bottom: {
            get: function() {
                return this.y + this.height;
            },
            set: function(value) {
                this.y = value - this.height;
            }
        },
        left: {
            get: function() {
                return this.y;
            },
            set: function(value) {
                this.y = value;
            }
        },
        centerX: {
            get: function() {
                return this.x + this.width / 2;
            },
            set: function(value) {
                this.x = value - this.width / 2;
            }
        },
        centerY: {
            get: function() {
                return this.y + this.height / 2;
            },
            set: function(value) {
                return value - this.height / 2;
            }
        }
    });
}