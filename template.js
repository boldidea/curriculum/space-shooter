// ------------------
//  Global Variables
// ------------------
let screenWidth = 600;
let screenHeight = 800;

let assets = {};

let ship;

// ----------------
//  Game Functions
// ----------------

function preload() {
    // This function runs automatically at the start of the game. 
    // This is where you load all of your assets (images, animations, and sounds).
    assets.ship = loadImage('assets/playerShip1_blue.png');
}

function setup() {
    // This function runs automatically at the start of the game, just after preload().
    createCanvas(screenWidth, screenHeight);

    // Create the ship
    ship = createSprite(assets.ship);
    ship.centerX = screenWidth / 2;
    ship.bottom = screenHeight - 20;
}

function draw() {
    // This function is run every frame, which is usually around 60 times per second
    background('black');
    drawSprites();
}

// Enable IDE autocompletion (intellisense) for p5.js:
/// <reference path="./p5.d.ts" />