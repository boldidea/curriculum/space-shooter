// Global variables
const SCREEN_WIDTH = 600;
const SCREEN_HEIGHT = 800;

let x = 10;
let y = 10;

function setup() {
    createCanvas(SCREEN_WIDTH, SCREEN_HEIGHT);
}

function drawSquare() {
    fill("red");
    x += 5;
    y += 5;
    rect(x, y, 50, 50);
}

function drawText() {
    textSize(20);
    fill("white");
    text("Hello, world!", 50, 50);
}

function draw() {
    background("black");
    drawSquare();
    drawText();
}

// Enable IDE autocompletion (intellisense) for p5.js:
/// <reference path="./p5.d.ts" />