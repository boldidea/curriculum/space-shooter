let screenWidth = 600;
let screenHeight = 800;

let assets = {};

let ship, meteors, lasers;

function preload() {
    // images
    assets.ship = loadImage("assets/playerShip1_blue.png");
    assets.meteor = loadImage("assets/meteorBrown_med1.png");
    assets.laser = loadImage("assets/laserBlue01.png");

    // animations
    assets.sonicExplosion = loadAnimation("assets/sonicExplosion00.png", "assets/sonicExplosion08.png");
}

function setup() {
    createCanvas(screenWidth, screenHeight);

    // Create the ship
    ship = createSprite(assets.ship);
    ship.centerX = screenWidth / 2;
    ship.bottom = screenHeight - 20;
    ship.addAnimation("explosion", assets.sonicExplosion);
    ship.setCollider("circle");

    // Create the sprite group for meteors
    meteors = createGroup();

    // Create the sprite group for lasers
    lasers = createGroup();
}

function updateShip() {
    if (keyIsDown(KEY.LEFT_ARROW)) {
        ship.x -= 10;
    }
    if (keyIsDown(KEY.RIGHT_ARROW)) {
        ship.x += 10;
    }

    // don"t allow ship to go out of bounds
    if (ship.left < 0) {
        ship.left = 0;
    }
    if (ship.right > screenWidth) {
        ship.right = screenWidth;
    }

    // Handle collisions with meteors
    ship.overlap(meteors, handleShipMeteorCollision);
}

function createLaser() {
    let laser = createSprite(assets.laser);
    laser.centerX = ship.centerX;
    laser.bottom = ship.top;
    laser.setSpeed(20);
    laser.setDirection(270);
    lasers.add(laser);
}

function createMeteor() {
    // Create a meteor
    let meteor = createSprite(assets.meteor);
    meteor.centerX = random(0, screenWidth);
    meteor.bottom = 0;
    meteor.setSpeed(random(1, 10));
    meteor.setDirection(random(60, 120));
    meteor.rotationSpeed = random(-2, 2);
    meteor.setCollider("circle");

    // Add the meteor to the meteors group
    meteors.add(meteor);
}

function updateMeteors() {
    // create one meteor every 60 frames
    if (frameCount % 60 === 0) {
        createMeteor();
    }

    // remove meteors that have gone off-screen
    for (let meteor of meteors) {
        if (meteor.top > screenHeight) {
            meteor.remove();
        }
    }
}

function updateLasers() {
    // fire a laser if the space key was pressed
    if (keyWentDown(KEY.SPACE)) {
        createLaser();
    }

    // remove lasers that have gone off-screen
    for (let laser of lasers) {
        if (laser.bottom < 0) {
            laser.remove();
        }
    }
}

function handleShipMeteorCollision(ship, meteor) {
    ship.changeAnimation("explosion");
    ship.animation.onComplete = shipFinishedExploding;
    meteor.remove();
}

function shipFinishedExploding() {
    ship.changeAnimation("normal");
}

function showSpriteCount() {
    textSize(12);
    fill("white");
    text(`sprite count: ${allSprites.length}`, 10, screenHeight - 10);
}

function draw() {
    background("black");

    updateShip();
    updateLasers();
    updateMeteors();

    drawSprites();
    showSpriteCount();
}

// Enable IDE autocompletion (intellisense) for p5.js:
/// <reference path="./p5.d.ts" />
