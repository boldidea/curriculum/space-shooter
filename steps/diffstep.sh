#!/bin/bash

diffstep() {
    clear;
    num="$1";
    prevnum=$(( num - 1));
    padnum=$(printf '%02d' $num);
    padprevnum=$(printf '%02d' $prevnum);
    file="step${padnum}.js";
    if [[ -f "$file" ]]; then
        [[ "$padprevnum" == "00" ]] && prevfile=/dev/null || prevfile="step${padprevnum}.js";
        diff -u $prevfile $file;
        echo
    else
        echo "No such file: $file";
    fi
    cmd="";
    while [[ ! "$cmd" =~ [qpn] ]]; do
        echo -en "\r[p]rev [n]ext [q]uit: ";
        read -sn 1 cmd;
    done
    [[ "$cmd" == "q" ]] && echo && exit 0;
    [[ "$cmd" == "p" ]] && diffstep $(( num - 1 ));
    [[ "$cmd" == "n" ]] && diffstep $(( num + 1 ));
}

[[ -n "$1" ]] && step="$1" || step="1";
diffstep "$step";