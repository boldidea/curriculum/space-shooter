let screenWidth = 600;
let screenHeight = 800;

let assets = {};

let ship, meteors;

function preload() {
    assets.ship = loadImage("assets/playerShip1_blue.png");
    assets.meteor = loadImage("assets/meteorBrown_med1.png");
}

function setup() {
    createCanvas(screenWidth, screenHeight);

    // Create the ship
    ship = createSprite(assets.ship);
    ship.centerX = screenWidth / 2;
    ship.bottom = screenHeight - 20;

    // Create the sprite group for meteors
    meteors = createGroup();
}

function updateShip() {
    if (keyIsDown(KEY.LEFT_ARROW)) {
        ship.x -= 10;
    }
    if (keyIsDown(KEY.RIGHT_ARROW)) {
        ship.x += 10;
    }

    // don"t allow ship to go out of bounds
    if (ship.left < 0) {
        ship.left = 0;
    }
    if (ship.right > screenWidth) {
        ship.right = screenWidth;
    }

    // Handle collisions with meteors
    ship.overlap(meteors, handleShipMeteorCollision);
}

function createMeteor() {
    // Create a meteor
    let meteor = createSprite(assets.meteor);
    meteor.centerX = random(0, screenWidth);
    meteor.bottom = 0;
    meteor.setSpeed(random(1, 10));
    meteor.setDirection(random(60, 120));
    meteor.rotationSpeed = random(-2, 2);

    // Add the meteor to the meteors group
    meteors.add(meteor);
}

function updateMeteors() {
    // create one meteor every 60 frames
    if (frameCount % 60 === 0) {
        createMeteor();
    }

    // remove meteors that have gone off-screen
    for (let meteor of meteors) {
        if (meteor.top > screenHeight) {
            meteor.remove();
        }
    }
}

function handleShipMeteorCollision(ship, meteor) {
    console.log("Ship is touching a meteor");
    meteor.remove();
}

function showSpriteCount() {
    textSize(12);
    fill("white");
    text(`sprite count: ${allSprites.length}`, 10, screenHeight - 10);
}

function draw() {
    background("black");

    updateShip();
    updateMeteors();

    drawSprites();
    showSpriteCount();
}

// Enable IDE autocompletion (intellisense) for p5.js:
/// <reference path="./p5.d.ts" />
