let screenWidth = 600;
let screenHeight = 800;

let assets = {};

let ship;

function preload() {
    assets.ship = loadImage("assets/playerShip1_blue.png");
}

function setup() {
    createCanvas(screenWidth, screenHeight);

    // Create the ship
    ship = createSprite(assets.ship);
    ship.centerX = screenWidth / 2;
    ship.bottom = screenHeight - 20;
}

function updateShip() {
    if (keyIsDown(KEY.LEFT_ARROW)) {
        ship.x -= 10;
    }
    if (keyIsDown(KEY.RIGHT_ARROW)) {
        ship.x += 10;
    }

    // don"t allow ship to go out of bounds
    if (ship.left < 0) {
        ship.left = 0;
    }
    if (ship.right > screenWidth) {
        ship.right = screenWidth;
    }
}

function draw() {
    background("black");

    updateShip();

    drawSprites();
}

// Enable IDE autocompletion (intellisense) for p5.js:
/// <reference path="./p5.d.ts" />
