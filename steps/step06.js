let screenWidth = 600;
let screenHeight = 800;

let assets = {};

let ship;

function preload() {
    assets.ship = loadImage("assets/playerShip1_blue.png");
    assets.meteor = loadImage("assets/meteorBrown_med1.png");
}

function setup() {
    createCanvas(screenWidth, screenHeight);

    // Create the ship
    ship = createSprite(assets.ship);
    ship.centerX = screenWidth / 2;
    ship.bottom = screenHeight - 20;
}

function updateShip() {
    if (keyIsDown(KEY.LEFT_ARROW)) {
        ship.x -= 10;
    }
    if (keyIsDown(KEY.RIGHT_ARROW)) {
        ship.x += 10;
    }

    // don"t allow ship to go out of bounds
    if (ship.left < 0) {
        ship.left = 0;
    }
    if (ship.right > screenWidth) {
        ship.right = screenWidth;
    }
}

function createMeteor() {
    // Create a meteor
    let meteor = createSprite(assets.meteor);
    meteor.centerX = random(0, screenWidth);
    meteor.bottom = 0;
    meteor.setSpeed(random(1, 10));
    meteor.setDirection(random(60, 120));
    meteor.rotationSpeed = random(-2, 2);
}

function draw() {
    background("black");

    updateShip();

    drawSprites();
}

// Enable IDE autocompletion (intellisense) for p5.js:
/// <reference path="./p5.d.ts" />
